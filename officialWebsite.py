import wx
import wx.html2


class OfficialWebsite(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.refresh_button = wx.Button(self, pos=(10, 10), size=(200, 30), label='丁香园官网')
        self.refresh_button.Bind(wx.EVT_BUTTON, self.onClickRefresh)
        self.about_button = wx.Button(self, pos=(220, 10), size=(200, 30), label='关于本应用')
        self.about_button.Bind(wx.EVT_BUTTON, self.onClickAbout)
        self.license_button = wx.Button(self, pos=(430, 10), size=(200, 30), label='开源许可证')
        self.license_button.Bind(wx.EVT_BUTTON, self.onClickLicense)
        self.browser = wx.html2.WebView.New(self, pos=(10, 50), size=(620, 390),
                                            url='https://ncov.dxy.cn/ncovh5/view/pneumonia')

    def onClickRefresh(self, event):
        self.browser = wx.html2.WebView.New(self, pos=(10, 50), size=(620, 390),
                                            url='https://ncov.dxy.cn/ncovh5/view/pneumonia')

    def onClickLicense(self, event):
        self.browser = wx.html2.WebView.New(self, pos=(10, 50), size=(620, 390),
                                            url='https://jxself.org/translations/gpl-3.zh.shtml')

    def onClickAbout(self, event):
        about = {
            'appname': 'pyCovid',
            'version': 'v1.1',
            'author': '森哥',
            'bilibili': '森哥__',
            'wechat': 'senge-x',
            'license': 'GPL-v3',
            'QQ': '1356872768'
        }
        wx.MessageBox(message=f"软件名：{about['appname']}\n"
                              f"版本号：{about['version']}\n"
                              f"作者：{about['author']}\n"
                              f"B站：{about['bilibili']}\n"
                              f"微信：{about['wechat']}\n"
                              f"QQ：{about['QQ']}\n"
                              f"软件许可证：{about['license']}\n"
                      )
