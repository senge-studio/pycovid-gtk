from networkCheck import NetworkCheck as net
import wx
import officialWebsite as data
import riskAreas as risk
import cnCovid as cn
import worldCovid as world


# 主界面
class PyCovid(wx.Frame):
    def __init__(self, parent, uid):
        wx.Frame.__init__(self, parent, uid, 'pyCovid新冠肺炎疫情数据', pos=(320, 240), size=(640, 480),
                          style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER)  # 禁止改变窗口大小
        panel = wx.Panel(self)
        self.icon = wx.Icon('covid.png', wx.BITMAP_TYPE_ICO)
        self.SetIcon(self.icon)
        notebook = wx.Notebook(panel)
        tab1 = cn.CnCovid(notebook)
        tab2 = world.WorldCovid(notebook)
        tab3 = data.OfficialWebsite(notebook)
        tab4 = risk.RiskAreas(notebook)
        notebook.AddPage(tab1, "国内疫情")
        notebook.AddPage(tab2, "全球疫情")
        notebook.AddPage(tab3, "官方网站")
        notebook.AddPage(tab4, "国内风险地区")
        sizer = wx.BoxSizer()
        sizer.Add(notebook, 1, wx.EXPAND)
        panel.SetSizer(sizer)


if __name__ == '__main__':
    # print('正在连接网络')
    network_status = net()
    if network_status:
        # print('网络连接成功，正在加载界面')
        app = wx.App()
        frame = PyCovid(parent=None, uid=-1)
        frame.Show()
        app.MainLoop()
    else:
        app = wx.App()
        wx.MessageBox(f'网络连接失败！错误代码：{network_status}')
        exit(1)
